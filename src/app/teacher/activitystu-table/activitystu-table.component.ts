import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivitystuTableDataSource, ActivitystuTableItem } from './activitystu-table-datasource';

@Component({
  selector: 'app-activitystu-table',
  templateUrl: './activitystu-table.component.html',
  styleUrls: ['./activitystu-table.component.css']
})
export class ActivitystuTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<ActivitystuTableItem>;
  dataSource: ActivitystuTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['teacher', 'id', 'name', 'location', 'description', 'periodOfRegitration', 'time', 'actions', 'update'];

  ngOnInit() {
    this.dataSource = new ActivitystuTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
