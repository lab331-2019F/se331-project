import { Component } from '@angular/core';
@Component({
  selector: 'app-teacher-update',
  templateUrl: './teacher.update.component.html',
  styleUrls: ['./teacher.update.component.css']
})
export class TeacherUpdateComponent {

  confirm() {
    if (confirm("Update Successfully ! ")) {
      window.location.pathname = '/activitystu'
    }
  }
}
