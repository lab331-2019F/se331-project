import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface ActivitydetailTableItem {
  id: number;
  activityName: string;
  studentName: string;
  time: string;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: ActivitydetailTableItem[] = [
  { id: 1, studentName: 'Hydrogen', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 2, studentName: 'Helium', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 3, studentName: 'Lithium', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 4, studentName: 'Beryllium', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 5, studentName: 'Boron', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 6, studentName: 'Carbon', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 7, studentName: 'Nitrogen', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 8, studentName: 'Oxygen', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 9, studentName: 'Fluorine', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 10, studentName: 'Neon', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 11, studentName: 'Sodium', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 12, studentName: 'Magnesium', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 13, studentName: 'Aluminum', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 14, studentName: 'Silicon', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 15, studentName: 'Phosphorus', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 16, studentName: 'Sulfur', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 17, studentName: 'Chlorine', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
  { id: 18, studentName: 'Argon', activityName: 'Basketball Competition', time: '24/9/2019 12.30pm' },
];

/**
 * Data source for the ActivitydetailTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ActivitydetailTableDataSource extends DataSource<ActivitydetailTableItem> {
  data: ActivitydetailTableItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<ActivitydetailTableItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() { }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: ActivitydetailTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: ActivitydetailTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'studentName': return compare(a.studentName, b.studentName, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
