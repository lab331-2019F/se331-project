import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivitydetailTableDataSource, ActivitydetailTableItem } from './activitydetail-table-datasource';

@Component({
  selector: 'app-activitydetail-table',
  templateUrl: './activitydetail-table.component.html',
  styleUrls: ['./activitydetail-table.component.css']
})
export class ActivitydetailTableComponent implements AfterViewInit, OnInit {
  [x: string]: any;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<ActivitydetailTableItem>;
  dataSource: ActivitydetailTableDataSource;

  isDisabled = false;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'studentName', 'activityName', 'time', 'status'];

  ngOnInit() {
    this.dataSource = new ActivitydetailTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }


  confirm(id: any) {
    if (confirm("Are you sure RNROLL this student to this activity ?")) {
      const button = <any>document.getElementById("disable_" + id);
      const button1 = <any>document.getElementById("disable1_" + id);
      button.disabled = true;
      button1.disabled = true;

    }
  }
  reject(id: any) {
    if (confirm("Are you sure REJECT this student into activity ?")) {
      const button = <any>document.getElementById("disable_" + id);
      const button1 = <any>document.getElementById("disable1_" + id);
      button.disabled = true;
      button1.disabled = true;

    }
  }
}
