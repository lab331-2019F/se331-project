import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from '../entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from '../service/activity-service';


@Component({
  selector: 'app-activity-table',
  templateUrl: './activity-table.component.html',
  styleUrls: ['./activity-table.component.css']
})


export class ActivityTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  // displayedColumns = ["id"];


  displayedColumns = ['id', 'name', 'location', 'description', 'periodOfRegitration', 'time', 'teacher', 'actions'];
  constructor(private activityService: ActivityService) { }
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;

  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activities => {
        this.dataSource = new ActivityTableDataSource();



        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.activities = this.activities;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }

  ngAfterViewInit() {

  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  confirm() {
    if (confirm("Are you sure to enroll this activity ?")) {
      window.location.pathname = '/enrolledPage'
    }
  }

}
