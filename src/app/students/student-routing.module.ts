import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { NgModule } from '@angular/core';
import { StudentTableComponent } from './student-table/student-table.component';
import { StudentsAddComponent } from '../administrator/add/administrator.add.component';
import { StudentsFillComponent } from './add/students.add.component';
import { AdministratorComponent } from '../administrator/administrator.component';
import { ActivityTableComponent } from '../activity-table/activity-table.component';
import { LoginComponent } from './login/students.login.component';
import { EnrolledPageTableComponent } from '../enrolledPage-table/enrolledPage-table.component';
import { TeacherComponent } from '../teacher/teacher.component';
import { ActivitystuTableComponent } from '../teacher/activitystu-table/activitystu-table.component';
import { ActivitydetailTableComponent } from '../teacher/activitydetail-table/activitydetail-table.component';
import { ListTableComponent } from '../administrator/list-table/list-table.component';
import { StudentListTableComponent } from '../administrator/student-list-table/student-list-table.component';
import { TeacherUpdateComponent } from '../teacher/update/teacher.update.component';
import { StudentsUpdateComponent } from '../UpdateStuInfo/students.update.component';
import { WelcomeComponent } from '../welcome/welcome.component';



const StudentRoutes: Routes = [
    { path: 'add', component: StudentsAddComponent },
    { path: 'list', component: StudentTableComponent },
    { path: 'table', component: StudentTableComponent },
    { path: 'administrator', component: AdministratorComponent },
    { path: 'student', component: StudentsFillComponent },
    { path: 'activity', component: ActivityTableComponent },
    { path: 'adminList', component: ListTableComponent },
    { path: 'login', component: LoginComponent },
    { path: 'enrolledPage', component: EnrolledPageTableComponent },
    { path: 'studentList', component: StudentListTableComponent },
    { path: 'teacher', component: TeacherComponent },
    { path: 'activitystu', component: ActivitystuTableComponent },
    { path: 'detail', component: ActivitydetailTableComponent },
    { path: 'update', component: TeacherUpdateComponent },
    {path:'UpdateStu',component: StudentsUpdateComponent},
    {path:'welcome',component: WelcomeComponent}


];
@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {

}
