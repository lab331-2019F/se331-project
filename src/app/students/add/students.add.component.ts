import { Component } from '@angular/core';
import Student from '../../entity/student';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsFillComponent {
  students: Student[];

  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }

  confirm(){
    if(confirm("Sign Up Successfully ! ")) {
      window.location.pathname = '/login'
    }
  }

}
