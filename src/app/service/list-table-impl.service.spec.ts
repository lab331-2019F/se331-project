import { TestBed } from '@angular/core/testing';

import { ListTableImplService } from './list-table-impl.service';

describe('ListTableImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListTableImplService]
    });
  });

  it('should be created', () => {
    const service: ListTableImplService = TestBed.get(ListTableImplService);
    expect(service).toBeTruthy();
  });
});
