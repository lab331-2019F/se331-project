import { TestBed } from '@angular/core/testing';
import { ActivityTableImplService } from './activity-table-impl.service';


describe('ActivityTableImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityTableImplService]
    });
  });
  it('should be created', () => {
    const service: ActivityTableImplService = TestBed.get(ActivityTableImplService);
    expect(service).toBeTruthy();
  });

});
