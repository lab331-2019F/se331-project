import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import Activity from '../entity/activity';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ActivityService } from './activity-service';


@Injectable({
  providedIn: 'root'
})
export  abstract class ActivityFileImplService extends ActivityService{
  getActivity(id: number): Observable<Activity> {
    return this.http.get<Activity[]>('assets/activity-s.json')
      .pipe(map(activities => {
        const output: Activity = (activities as Activity[]).find(activity => activity.id === +id);
        return output;
      }));

  }
  constructor(private http: HttpClient) {
    super();
  }
  getActivitys(): Observable<Activity[]> {
    return this.http.get<Activity[]>('assets/activity-s.json');
  }
  saveActivity(student: Activity):Observable<Activity>{
    return null;
  }
}
