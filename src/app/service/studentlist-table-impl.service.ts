import { Injectable } from '@angular/core';
import { ActivityService } from './activity-service';
import { HttpClient } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export abstract class StudentlistTableImplService extends ActivityService{

  constructor(private http: HttpClient) {
    super();
  }
}
