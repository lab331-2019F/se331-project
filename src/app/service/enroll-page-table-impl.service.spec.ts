import { TestBed } from '@angular/core/testing';

import { EnrollPageTableImplService } from './enroll-page-table-impl.service';

describe('EnrollPageTableImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnrollPageTableImplService]
    });
  });

  it('should be created', () => {
    const service: EnrollPageTableImplService = TestBed.get(EnrollPageTableImplService);
    expect(service).toBeTruthy();
  });
});
