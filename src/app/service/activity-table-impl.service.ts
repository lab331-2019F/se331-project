import { Injectable } from '@angular/core';
import { ActivityService } from './activity-service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export abstract class ActivityTableImplService extends ActivityService {

  constructor(private http: HttpClient) {
    super();
  }
}
