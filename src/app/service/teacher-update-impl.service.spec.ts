import { TestBed } from '@angular/core/testing';

import { TeacherUpdateImplService } from './teacher-update-impl.service';

describe('TeacherUpdateImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeacherUpdateImplService]
    });
  });

  it('should be created', () => {
    const service: TeacherUpdateImplService = TestBed.get(TeacherUpdateImplService);
    expect(service).toBeTruthy();
  });
});
