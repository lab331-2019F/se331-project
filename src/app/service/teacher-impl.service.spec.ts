import { TestBed } from '@angular/core/testing';

import { TeacherImplService } from './teacher-impl.service';

describe('TeacherImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeacherImplService]
    });
  });

  it('should be created', () => {
    const service: TeacherImplService = TestBed.get(TeacherImplService);
    expect(service).toBeTruthy();
  });
});
