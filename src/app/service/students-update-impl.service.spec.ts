import { TestBed } from '@angular/core/testing';

import { StudentsUpdateImplService } from './students-update-impl.service';

describe('StudentsUpdateImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentsUpdateImplService]
    });
  });

  it('should be created', () => {
    const service: StudentsUpdateImplService = TestBed.get(StudentsUpdateImplService);
    expect(service).toBeTruthy();
  });
});
