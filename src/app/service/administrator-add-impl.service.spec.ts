import { TestBed } from '@angular/core/testing';

import { AdministratorAddImplService } from './administrator-add-impl.service';

describe('AdministratorAddImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdministratorAddImplService]
    });
  });

  it('should be created', () => {
    const service: AdministratorAddImplService = TestBed.get(AdministratorAddImplService);
    expect(service).toBeTruthy();
  });
});
