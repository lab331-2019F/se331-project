import { TestBed } from '@angular/core/testing';

import { ActivitystuTableImplService } from './activitystu-table-impl.service';

describe('ActivitystuTableImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivitystuTableImplService]
    });
  });
  it('should be created', () => {
    const service: ActivitystuTableImplService = TestBed.get(ActivitystuTableImplService);
    expect(service).toBeTruthy();
  });
});
