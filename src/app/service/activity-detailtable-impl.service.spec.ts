import { TestBed } from '@angular/core/testing';

import { ActivityDetailtableImplService } from './activity-detailtable-impl.service';
import { inject } from '@angular/core';

describe('ActivityDetailtableImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityDetailtableImplService]
    });
  });
  it('should be created', () => {
    const service: ActivityDetailtableImplService = TestBed.get(ActivityDetailtableImplService);
    expect(service).toBeTruthy();
  });

});
