import { TestBed } from '@angular/core/testing';

import { StudentlistTableImplService } from './studentlist-table-impl.service';

describe('StudentlistTableImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentlistTableImplService]
    });
  });

  it('should be created', () => {
    const service: StudentlistTableImplService = TestBed.get(StudentlistTableImplService);
    expect(service).toBeTruthy();
  });
});
