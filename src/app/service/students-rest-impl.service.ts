import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable } from 'rxjs';
import Student from '../entity/student';
//import { HttpClient } from 'selenium-webdriver/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export  class StudentsRestImplService extends StudentService{

  constructor(private http: HttpClient) {
    super();
   }

   getStudents():Observable<Student[]>{
     return null;
   }

   getStudent(id:number): Observable<Student>{
    return null;

  }
  
}
