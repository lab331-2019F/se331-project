import { Component } from '@angular/core';
import Student from '../entity/student';

@Component({
  selector: 'app-students-update',
  templateUrl: './students.update.component.html',
  styleUrls: ['./students.update.component.css']
})
export class StudentsUpdateComponent {
  students: Student[];



  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }

  confirm() {
    if (confirm("Are you sure want to update ?")) {
      window.location.pathname = '/activity'
    }
  }

}
