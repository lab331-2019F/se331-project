import { Component, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTable } from '@angular/material';
import { EnrolledPageTableItem, EnrolledPageTableDataSource } from './enrolledPage-table-datasource';
import Activity from '../entity/activity';



@Component({
  selector: 'app-enrolledPage-table',
  templateUrl: './enrolledPage-table.component.html',
  styleUrls: ['./enrolledPage-table.component.css']
})
export class EnrolledPageTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<EnrolledPageTableItem>;
  dataSource: EnrolledPageTableDataSource;
  activities: Activity;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id','status','name', 'location', 'description', 'periodOfRegitration', 'time', 'teacher'];

  ngOnInit() {
    this.dataSource = new EnrolledPageTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
  
}
