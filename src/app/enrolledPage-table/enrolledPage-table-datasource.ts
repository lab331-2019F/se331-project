import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface EnrolledPageTableItem {
  id: number;
  name: string;
  location: string;
  description: string;
  periodOfRegitration: string;
  time: string;
  teacher: string;
  status:string;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: EnrolledPageTableItem[] = [
  { id: 1, name: 'Basketball Competition', location: 'Main Gymnasium', description: '4 set (15 mins per set) have a break in set 2nd 10mins', periodOfRegitration: '22/8/2019 - 22/9/2019', time: '24/9/2019 12.30pm', teacher: "Aj.QQ",status:'Pending'},
  { id: 2, name: 'Mother’s Day', location: 'CAMT', description: ' The Queens birthdays is considered as Mother’s Day.', periodOfRegitration: '22/10/2019 - 23/10/2019', time: '5/11/2019 11:00am', teacher: 'Aj.DD',status:'Pending'}

];

/**
 * Data source for the StudentTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class EnrolledPageTableDataSource extends DataSource<EnrolledPageTableItem> {
  data: EnrolledPageTableItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<EnrolledPageTableItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() { }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: EnrolledPageTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: EnrolledPageTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
