export default class Activity {
    id: number;
    name: string;
    location: string;
    description: string;
    periodOfRegitration: string;
    time: string;
    teacher: string;
    studentName:string;
    activityName:string;
}
