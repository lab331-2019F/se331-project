import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsComponent } from './students/list/students.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule
} from '@angular/material';
import { MatInputModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { StudentsAddComponent } from './administrator/add/administrator.add.component';
import { StudentsRestImplService } from './service/students-rest-impl.service';
import { StudentsFillComponent } from './students/add/students.add.component';
import { LoginComponent } from './students/login/students.login.component';
import { ActivityTableComponent } from './activity-table/activity-table.component';
import { EnrolledPageTableComponent } from './enrolledPage-table/enrolledPage-table.component';
import { TeacherComponent } from './teacher/teacher.component';
import { ActivitystuTableComponent } from './teacher/activitystu-table/activitystu-table.component';
import { ActivitydetailTableComponent } from './teacher/activitydetail-table/activitydetail-table.component';
import Activity from './entity/activity';
import { ActivityService } from './service/activity-service';
import { ActivityFileImplService } from './service/activity-file-impl.service';
import { ListTableComponent } from './administrator/list-table/list-table.component';
import { StudentListTableComponent } from './administrator/student-list-table/student-list-table.component';
import { TeacherUpdateComponent } from './teacher/update/teacher.update.component';
import { StudentsUpdateComponent } from './UpdateStuInfo/students.update.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    AdministratorComponent,
    StudentsFillComponent,
    LoginComponent,
    ActivityTableComponent,
    StudentTableComponent,
    EnrolledPageTableComponent,
    TeacherComponent,
    ActivitystuTableComponent,
    ActivitydetailTableComponent,
    ListTableComponent,
    ListTableComponent,
    StudentListTableComponent,
    TeacherUpdateComponent,
    StudentsUpdateComponent,
    WelcomeComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    StudentRoutingModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [
    { provide: ActivityService, useExisting: ActivityFileImplService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
