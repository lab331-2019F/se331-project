import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface ListTableItem {
  id: number;
  name: string;
  location: string;
  description: string;
  periodOfRegitration: string;
  time: string;
  teacher: string;

}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: ListTableItem[] = [
  { id: 1, name: 'Basketball Competition', location: 'Main Gymnasium', description: '4 set (15 mins per set) have a break in set 2nd 10mins', periodOfRegitration: '22/8/2019 - 22/9/2019', time: '24/9/2019 12.30pm', teacher: "Aj.QQ" },
  { id: 2, name: 'Loy Krathong', location: 'CAMT', description: 'We will make Krathong together ', periodOfRegitration: '22/10/2019 - 23/10/2019 ', time: '23/11/2019 3:00pm', teacher: 'Aj.AA' },
  { id: 3, name: 'Songkran Festival', location: 'Dan Sai Town', description: 'people use buckets,water guns, and anything else that they can get their hands on to use to splash water on others.', periodOfRegitration: '13/3/2019 - 15/3/2019', time: '13/4/2019 10:00am', teacher: 'Aj.CC' },
  { id: 4, name: 'Phuket Vegetarian Festival', location: 'Phuket, Thailand', description: 'This is one of the major festivals in Thailand and during a month before it, people abstain themselves from consuming meat, garlic, onions, etc.', periodOfRegitration: '28/8/2019 - 30/18/2019', time: '28/9/2019 8:00am', teacher: 'Aj.QQ' },
  { id: 5, name: 'Mother’s Day', location: 'CAMT', description: ' The Queens birthdays is considered as Mother’s Day.', periodOfRegitration: '22/10/2019 - 23/10/2019', time: '5/11/2019 11:00am', teacher: 'Aj.DD' }

];

/**
 * Data source for the ActivityListTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ListTableDataSource extends DataSource<ListTableItem> {
  data: ListTableItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<ListTableItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() { }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: ListTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: ListTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
