import { Component } from '@angular/core';
import Student from '../../entity/student';

@Component({
  selector: 'app-administrator-add',
  templateUrl:'./administrator.add.component.html',
  styleUrls: ['./administrator.add.component.css']
})
export class StudentsAddComponent {
  students: Student[];

  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }

  showActivityList(){
    window.location.pathname = '/adminList'
  }

}
