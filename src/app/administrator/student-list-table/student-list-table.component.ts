import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { StudentListTableDataSource, StudentListTableItem } from './student-list-table-datasource';

@Component({
  selector: 'app-student-list-table',
  templateUrl: './student-list-table.component.html',
  styleUrls: ['./student-list-table.component.css']
})
export class StudentListTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<StudentListTableItem>;
  dataSource: StudentListTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id','status','username','name','surname','image','DOB','studentId'];

  ngOnInit() {
    this.dataSource = new StudentListTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }

  confirm(id: any) {
    if (confirm("Are you sure 'Comfirm' this student REGISTER this system ?")) {
      const button = <any>document.getElementById("disable_" + id);
      const button1 = <any>document.getElementById("disable1_" + id);
      button.disabled = true;
      button1.disabled = true;

    }
  }
  reject(id: any) {
    if (confirm("Are you sure 'Reject' this student REGISTER this system ?")) {
      const button = <any>document.getElementById("disable_" + id);
      const button1 = <any>document.getElementById("disable1_" + id);
      button.disabled = true;
      button1.disabled = true;

    }
  }
}
