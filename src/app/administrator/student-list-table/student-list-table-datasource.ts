import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface StudentListTableItem {
  id: number;
  name: string;
  surname: string;
  image: string;
  DOB: string;
  studentId: string;
  username: string;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: StudentListTableItem[] = [
  {
    id: 1,
    name: "Xingyuan",
    surname: "Kang",
    image: "https://scontent.fbkk12-4.fna.fbcdn.net/v/t1.0-9/70507211_449562805646151_1337190187314184192_o.jpg?_nc_cat=110&_nc_eui2=AeG68uKKtRrrOA4VL1LYSn2l9s-dtGOPQtaPI4QVRKhS3UeunQUU8AdVNKSNfHYQAH0HPvrKqgryq2ac-WuGSe7Bu-wA1yUfOBtxuLPIzF5Gkg&_nc_oc=AQn_QVMZGyZuXYK_s17n8w8X-r3yBJTfsSxsUFFtD2HGi2q_R1il_gMr42N-mIzrkRM&_nc_ht=scontent.fbkk12-4.fna&oh=6c08986a2ce74c7e29908d6bb8409960&oe=5E004FCD",
    DOB: "1998-12-31",
    studentId: "602115517",
    username: "myaploy@gmail.com"
  },
  {
    id: 2,
    name: "Youmei",
    surname: "Fan",
    image: "https://scontent.fbkk12-2.fna.fbcdn.net/v/t1.0-9/69521595_463806400888941_7846271020561334272_n.jpg?_nc_cat=104&_nc_oc=AQlYiFL47tDqmMrX_kFRJVNHX0ixgY504m5jSR3nFttlpH7uH6Dli5Xy__AHoOJk5yw&_nc_ht=scontent.fbkk12-2.fna&oh=0030cf5b3bdcea835d81919baad9fcd3&oe=5E311877",
    DOB: "1998-06-10",
    studentId: "602115522",
    username: "youmeifan99@gmail.com"
  },
  {
    id: 3,
    name: " Taylor",
    surname: "Swift",
    image: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1569268402300&di=32aabcd20f671a4aae18a5641ff40764&imgtype=0&src=http%3A%2F%2Fdingyue.nosdn.127.net%2FtFEODxm4UIo13ltopuuXXJlFDYRKwR5atXkklqTEtBIZg1526287018884.jpeg",
    DOB: "1989-12-13",
    studentId: "602115555",
    username: "taylor@gmail.com"
  },
  {
    id: 4,
    name: "Edward",
    surname: "Sheeran",
    image: "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3458365475,1797344011&fm=26&gp=0.jpg",
    DOB: "1991-02-17",
    studentId: "602115533",
    username: "edward@gmail.com"
  },
  {
    id: 5,
    name: "Chris",
    surname: "Evans",
    image: "http://www.gstatic.com/tv/thumb/persons/173489/173489_v9_ba.jpg",
    DOB: "1981-06-13",
    studentId: "602115566",
    username:"chris@gmail.com"
  }
];

/**
 * Data source for the StudentListTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class StudentListTableDataSource extends DataSource<StudentListTableItem> {
  data: StudentListTableItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<StudentListTableItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() { }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: StudentListTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: StudentListTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
